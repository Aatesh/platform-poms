# Atlassian Platform POMs

## Usage

In your project's parent pom, import via dependency management:

    <properties>
        <platform.version>2.22.7</platform.version>
    </properties>
     
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>platform</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.atlassian.platform</groupId>
                <artifactId>third-party</artifactId>
                <version>${platform.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

In your modules, import the desired dependencies with a scope, but not a version e.g.

    <dependencies>
        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
    
## Known Issues

When using maven 2.1.0: dependencies that use import scoped pom artifacts cannot resolve that pom artifact. See http://jira.codehaus.org/browse/MNG-3553

## Atlassian Developer?

### Committing Guidelines

Upon release of a new version of a [Platform Module (go/platform)](http://go.atlassian.com/platform), please push the version to the appropriate branch. No PR is required.

Please discuss with the Armata team about which branch to push to, see [Platform Rules Of Engagement (go/proe)](http://go.atlassian.com/proe).

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/JPP)

### Releasing

Releasing is done with a release stage in the bamboo CI build.
See the [Platform Rules Of Engagement](http://go.atlassian.com/proe) for details.

## External User?

### Issues

Please raise any issues you find with this module in [JIRA](https://ecosystem.atlassian.net/browse/PLATFORM)